FROM debian:9 AS build

RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev
RUN wget https://nginx.org/download/nginx-1.19.4.tar.gz && \
tar xvfz nginx-1.19.4.tar.gz && \
cd nginx-1.19.4 && \
./configure && \
make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
#COPY --from=build /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/
COPY --from=build /usr/local/nginx/conf/mime.types /usr/local/nginx/conf/
CMD ["./nginx", "-g", "daemon off;"]